package fonetime.primelite.com.demopjsua;

import org.pjsip.pjsua2.Account;
import org.pjsip.pjsua2.OnRegStartedParam;
import org.pjsip.pjsua2.OnRegStateParam;

/**
 * Created by piyush on 22/5/17.
 */

public class MyAccount extends Account{



    @Override
    public void onRegState(OnRegStateParam prm){
        System.out.println("regState " + prm.getCode() +" "+ prm.getReason()+" "+prm.getStatus()+" "+prm.getExpiration());
    }

    @Override
    public void onRegStarted(OnRegStartedParam prm) {
       System.out.println("registration Started");
    }



}
