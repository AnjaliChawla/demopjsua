package fonetime.primelite.com.demopjsua;

import android.os.Environment;

import java.io.File;

/**
 * Created by Anjali Chawla on 24/5/17.
 */

public class AppGlobal {

    public static File generateLogsOnSD(String sFileName) {
        java.io.File root = new File(Environment.getExternalStorageDirectory(), "PJ_Notes");
        if (!root.exists()) {
            root.mkdirs();
        }
        File file = new File(root, sFileName + ".txt");

        return file;
    }
}
