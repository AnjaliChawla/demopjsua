package fonetime.primelite.com.demopjsua;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.AccountRegConfig;
import org.pjsip.pjsua2.AuthCredInfo;
import org.pjsip.pjsua2.Endpoint;
import org.pjsip.pjsua2.EpConfig;
import org.pjsip.pjsua2.LogConfig;
import org.pjsip.pjsua2.LogEntry;
import org.pjsip.pjsua2.LogWriter;
import org.pjsip.pjsua2.TransportConfig;
import org.pjsip.pjsua2.TransportInfo;
import org.pjsip.pjsua2.UaConfig;
import org.pjsip.pjsua2.pj_log_decoration;
import org.pjsip.pjsua2.pjsip_transport_type_e;

/**
 * Created by piyush on 22/5/17.
 */

public class MyService extends Service {
    Endpoint ep;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            System.loadLibrary("pjsua2");

        } catch (Exception e) {
            System.out.print("lib Loading Exception : " + e.getMessage());
        }
        try {
            ep = new Endpoint();
            ep.libCreate();

            System.out.println("library loaded");

            EpConfig epConfig = new EpConfig();


	/* Set log config. */
            LogConfig log_cfg = epConfig.getLogConfig();
            log_cfg.setLevel(5);
            log_cfg.setConsoleLevel(5);
            log_cfg.setFilename(AppGlobal.generateLogsOnSD("sipLogFile").toString());
            log_cfg.setMsgLogging(5);


            MyLogWriter logWriter = new MyLogWriter();
            log_cfg.setWriter(logWriter);
            log_cfg.setDecor(log_cfg.getDecor() &
                    ~(pj_log_decoration.PJ_LOG_HAS_CR.swigValue() |
                            pj_log_decoration.PJ_LOG_HAS_NEWLINE.swigValue()));

     /*Set Ua Config. */
            UaConfig ua_cfg = epConfig.getUaConfig();

            ua_cfg.setUserAgent(getApplication().getPackageName() + ep.libVersion().getFull());


            ep.libInit(epConfig);
            TransportConfig transportConfig = new TransportConfig();
            transportConfig.setPort(7890);

            int i = ep.transportCreate(pjsip_transport_type_e.PJSIP_TRANSPORT_UDP, transportConfig);
            ep.libStart();
            TransportInfo transportInfo = ep.transportGetInfo(i);
            transportInfo.getInfo();
            AccountConfig accConfig = new AccountConfig();
            accConfig.setIdUri("sip:917042436116@85.13.213.94:7832");


            AccountRegConfig accountRegConfig = accConfig.getRegConfig();
            accountRegConfig.setRegistrarUri("sip:85.13.213.94:7832");
            accountRegConfig.setRegisterOnAdd(true); // register when added : by Default true
            accountRegConfig.setContactParams(";param=Hi%20there");
            accountRegConfig.setTimeoutSec(1000); // by default 300
            accountRegConfig.setDelayBeforeRefreshSec(7000); // by default 5000 ie 5 sec
//            accountRegConfig.setDropCallsOnFail();


            AuthCredInfo authCredInfo = new AuthCredInfo("digest", "*", "917042436116", 0, "12345678");
            accConfig.getSipConfig().getAuthCreds().add(authCredInfo);


            MyAccount acc = new MyAccount();
            acc.create(accConfig, true);


            System.out.println("reg-acc: StatusLineText : " + acc.getInfo().getRegStatusText());
            System.out.println("reg-acc: RegStatus : " + acc.getInfo().getRegStatus());
            System.out.println("reg-acc: getDelayedBeforeRefresh : " + accountRegConfig.getDelayBeforeRefreshSec());
            System.out.println("reg-acc: DropCallsOnFail : " + accountRegConfig.getDropCallsOnFail());
            System.out.println("reg-acc: Headers : " + accountRegConfig.getHeaders().size());

            System.out.println("reg-acc: IsActive : " + acc.getInfo().getRegIsActive());
            System.out.println("reg-acc: IsConfigured : " + acc.getInfo().getRegIsConfigured());


            Thread.sleep(10000);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onDestroy() {


        try {
            ep.libDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ep.delete();

    }

    class MyLogWriter extends LogWriter {
        @Override
        public void write(LogEntry entry) {
            System.out.println(entry.getMsg());
        }

    }

}
